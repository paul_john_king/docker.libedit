# © 2019 Paul John King (paul_john_king@web.de).  All rights reserved.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License, version 3 as published by the
# Free Software Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

ARG PROJECT_NAME
ARG PROJECT_VERSION
ARG PROJECT_URL

ARG WORKBENCH_IMAGE="registry.gitlab.com/paul_john_king/docker.ubuntu_workbench:0.0.18_7efec42"

ARG LIBEDIT_DOWNLOADS="http://thrysoee.dk/editline"
ARG LIBEDIT_VERSION="20190324-3.1"

ARG WORK_DIR="/work"
ARG DOWNLOAD_DIR="${WORK_DIR}/downloads"
ARG SOURCE_DIR="${WORK_DIR}/sources"
ARG TARGET_DIR="${WORK_DIR}/targets"

FROM "${WORKBENCH_IMAGE}" AS workbench

	ARG LIBEDIT_DOWNLOADS
	ARG LIBEDIT_VERSION

	ARG WORK_DIR
	ARG DOWNLOAD_DIR
	ARG SOURCE_DIR
	ARG TARGET_DIR

	ARG LIBEDIT_NAME="libedit-${LIBEDIT_VERSION}"

	ARG LIBEDIT_ARCHIVE_SUFFIX="tar.gz"
	ARG LIBEDIT_ARCHIVE_NAME="${LIBEDIT_NAME}.${LIBEDIT_ARCHIVE_SUFFIX}"
	ARG LIBEDIT_ARCHIVE_URL="${LIBEDIT_DOWNLOADS}/${LIBEDIT_ARCHIVE_NAME}"
	ARG LIBEDIT_ARCHIVE_PATH="${DOWNLOAD_DIR}/${LIBEDIT_ARCHIVE_NAME}"

	# TODO: Build and use own libncurses.  Or download and use libncurses
	# sources.

	RUN \
		set -e; \
		set -u; \
		apt-get install \
			"libncurses-dev=6.1+20181013-2ubuntu2"; \
		mkdir -p \
			"${DOWNLOAD_DIR}" \
			"${SOURCE_DIR}" \
			"${TARGET_DIR}"; \
		cd "${DOWNLOAD_DIR}"; \
		curl --remote-name "${LIBEDIT_ARCHIVE_URL}"; \
		cd "${SOURCE_DIR}"; \
		tar \
			--extract \
			--file="${LIBEDIT_ARCHIVE_PATH}" \
			--strip-components="1"; \
		./configure \
			--prefix="${TARGET_DIR}"; \
		make install; \
		return;

FROM "scratch"

	ARG PROJECT_NAME
	ARG PROJECT_VERSION
	ARG PROJECT_URL

	ARG WORKBENCH_IMAGE

	ARG LIBEDIT_DOWNLOADS
	ARG LIBEDIT_VERSION

	ARG TARGET_DIR

	LABEL \
		project.name="${PROJECT_NAME}" \
		project.version="${PROJECT_VERSION}" \
		project.url="${PROJECT_URL}" \
		workbench.image="${WORKBENCH_IMAGE}" \
		libedit.downloads="${LIBEDIT_DOWNLOADS}" \
		libedit.version="${LIBEDIT_VERSION}"

	COPY --from="workbench" "${TARGET_DIR}" "/"
